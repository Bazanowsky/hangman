import Parts from "./classes/Hangman/Parts";
import Word from "./classes/Hangman/Word";
import UsedLetters from "./classes/Hangman/UsedLetters";

class Hangman {
	constructor(options) {
		// TODO: options = defaultOptions;
		this.question = null;
		this.parts = new Parts(options.parts);
		this.word = new Word(options.word);
		this.usedLetters = new UsedLetters(options.usedLetters);
		this.input = options.input;
		this.answerForm = options.answerForm;


		this.registerEvents();		
	}

	registerEvents() {
		this.answerForm.addEventListener('submit', this.tryAnswer.bind(this));
		this.answerForm.addEventListener('click', () =>{ this.input.focus() });
	}

	start() {
		this.question = this.generateQuestion();
		this.word.start(this.question);
		this.parts.restart();
		this.usedLetters.restart();
	}

	generateQuestion() {
		let availableQuestions = [
			'aston martin',
			'ferrari',
			'porsche',
			'maybach',
			'mercedes',
		];
		let index = Math.floor(Math.random() * availableQuestions.length);
		return availableQuestions[index];
	}

	checkAnswer(answer) {
		let isCorrect = true;
		if (answer.length === 0) 
			return isCorrect;
		

		if (answer.length === 1) {
			isCorrect = this.usedLetters.isLetterAlreadyUsed(answer) || this.word.checkLetter(answer);
		} else {
			isCorrect = this.word.checkAnswer(answer);
		}

		return isCorrect;
	}

	tryAnswer(e) {
		e.preventDefault();
		let answer = this.input.value;
		this.input.value = null;

		let isCorrect = this.checkAnswer(answer);

		if (answer.length === 1) {
			this.usedLetters.addLetter(answer);
		}

		if (!isCorrect) {
			this.answerFailed();
		}

		if (this.checkWon(answer)) {
			this.gameWon();
		}
	}

	answerFailed() {
		this.parts.drawNextStep();
		if (!this.parts.isAlive()) {
			this.gameLose();
		}
	}

	gameLose() {
		setTimeout(()=>{

			alert('You lost!');

			this.start();
		}, 500);
	}

	gameWon() {
		this.word.showAllLetters();
		setTimeout(()=>{
			alert('You won!');

				this.start();
			}, 500);


	}

	checkWon(answer) {
		if (this.word.checkAnswer(answer)) 
			return true;
		
		if (this.word.allLettersVisible())
			return true;
		
		return false;
	}

	focusInput() {
		this.input.focus();
	}

}

export default Hangman;