import Hangman from "./Hangman";

(function(){
	
	let options = {
		parts: document.getElementById('hangman-parts'),
		word: document.getElementById('hangman-word'),
		input: document.getElementById('answer-input'),
		answerForm: document.getElementById('answer-form'),
		usedLetters: document.getElementById('used-letters'),
	}
	let hangman = new Hangman(options);
	hangman.start();

})();
