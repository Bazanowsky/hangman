class UsedLetters {
	constructor(container) {
		this.container = container;
		this.registerEvents();
		this.usedLetters = [];
	}

	registerEvents() {

	}

	addLetter(letter) {
		if (this.isLetterAlreadyUsed(letter))
			return;

		this.usedLetters.push(letter);
		let element = document.createElement('span');
		element.classList.add('letter');
		element.innerText = letter;
		element.HTML += ' ';
		this.container.appendChild(element);
	}
	
	isLetterAlreadyUsed(letter) {
		return this.usedLetters.indexOf(letter) > -1;
	}

	restart() {
		this.usedLetters = [];
		this.container.innerHTML = '';
	}

}

export default UsedLetters;