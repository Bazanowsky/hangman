class Word {
	constructor(container) {
		this.container = container;
		this.registerEvents();
	}

	registerEvents() {

	}

	start(word) {
		this.reset();
		this.rawQuestion = word;

		this.word = this.prepareWord(word);
	}

	prepareWord(word) {
		let wordArray = [];

		[...word].map((letter, index) => {
			let element = document.createElement('span');

			if (letter === ' ') {
				element.classList.add('spacebar');
			} else {
				element.classList.add('letter');
			}
			this.container.appendChild(element);

			wordArray.push({
				index: index, 
				letter: letter,
				visible: false,
				element: element
			});
		});

		return wordArray;
	}

	reset() {
		this.container.innerHTML = '';
	}

	checkLetter(letter) {
		let isCorrect = false;

		let positions =	this.getLetterPositions(letter);

		if (positions.length) {
			isCorrect = true;
			positions.forEach((i)=>{
				this.showLetter(i);
			})
		}

		return isCorrect;
	}

	showLetter(index) {
		this.word[index].visible = true;
		this.word[index].element.innerText = this.word[index].letter;
	}

	showAllLetters() {
		this.word.forEach((word,index)=>{
			this.showLetter(index);
		});
	}

	checkAnswer(answer) {
		return answer.toLowerCase() === this.rawQuestion.toLowerCase();
	}

	allLettersVisible() {
		let visible = true;
		this.word.forEach((item) =>{
			if (!item.visible) {
				visible = false;
			}
		})
		return visible;
	}

	getLetterPositions(letter) {
		let positions = [];
		let i = -1;
		while ((i = this.rawQuestion.indexOf(letter, i+1)) != -1){
			positions.push(i);
		}
		return positions;
	}
}

export default Word;