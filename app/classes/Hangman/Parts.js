class Parts {
	constructor(container) {
		this.container = container;

		this.nextStep = 0;
		this.steps = 0;
		this.alive = true;

		this.parts = this.getPartsFromContainer();
		this.registerEvents();
	}

	registerEvents() {

	}

	drawNextStep() {
		if (!this.isAlive())
			return; 

		let nextPart = this.parts[this.nextStep];
		nextPart.classList.add('active');
		this.nextStep++;

		if (this.isLastStep()) {
			this.killHim();
		}
	}


	getPartsFromContainer() {
		let parts = this.container.querySelectorAll('.hangman-part[data-step]');
		this.steps = parts.length;

		return parts;
	}

	killHim() {
		this.alive = false;
		this.container.classList.add('finished');
	}

	isAlive() {
		return this.alive;
	}

	isLastStep() {
		return this.nextStep == this.steps;
	}

	restart() {
		this.alive = true;
		this.nextStep = 0;
		Array.from(this.parts).map((part)=> part.classList.remove('active'));
		this.container.classList.remove('finished');
	}
}

export default Parts;